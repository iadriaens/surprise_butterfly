/* Escape box
Components:
Key 1: butterflies in my stomach
Key 2: temperature of love
Key 3: sing me a lovesong

*/
//---------------------LIBRARIES TO INCLUDE-------------------------------
#include <OneWire.h>
#include <DallasTemperature.h>

//----------------------DECLARE VARIABLES---------------------------------
// temperature
OneWire geelDraadje(A0);
DallasTemperature sensor(&geelDraadje);
int LedTemp = 2;

// morse
unsigned int LedMorse = 3;

// led vlinders
int LedVlinder1 = 12;
int invoerVlinder1 = 4;
int LedVlinder2 = 10;
int invoerVlinder2 = 6;
int LedVlinder3 = 11;
int invoerVlinder3 = 7;
int LedVlinder4 = 13;
int invoerVlinder4 = 9;

int invoer1;
int invoer2;
int invoer3;
int invoer4;

//------------------------SETUP--------------------------------------------
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  // start de sensor library
  sensor.begin();
  pinMode(LedTemp,OUTPUT);   // set up LED TEMP sensor

  // morse code
  pinMode(LedMorse,OUTPUT);   // set up LED MORSE
  //morsecode();

  // LED vlinders
  pinMode(LedVlinder1, OUTPUT);
  pinMode(invoerVlinder1,INPUT_PULLUP);
  pinMode(LedVlinder2, OUTPUT);
  pinMode(invoerVlinder2,INPUT_PULLUP);
  pinMode(LedVlinder3, OUTPUT);
  pinMode(invoerVlinder3,INPUT_PULLUP);
  pinMode(LedVlinder4, OUTPUT);
  pinMode(invoerVlinder4,INPUT_PULLUP);
}

//-------------------------CUSTOM FUNCTIONS---------------------------------
// function to control LED for TEMP
void ledbytemp(float T){
  // led = on if temp =  between 32.2 and 32.8°C
  if (T > 31.0 && T < 33.0){
    digitalWrite(LedTemp,HIGH);
  }
  else {
    digitalWrite(LedTemp, LOW);
  }
}

// functions to sign TRAPKAST with LED
void dot()
{
  digitalWrite(LedMorse, HIGH);
  delay(400);
  digitalWrite(LedMorse, LOW);
  delay(600);
}
void dash()
{
  digitalWrite(LedMorse, HIGH);
  delay(1200);
  digitalWrite(LedMorse, LOW);
  delay(600);
}
void morsecode()
{
  // morse code: sein trapkast
  dash();delay(1200); // T
  dot();dash();dot();delay(1200); // R
  dot();dash();delay(1200);  // A
  dash();dash();dot();delay(1200); // P
  dash();dot();dash();delay(1200); // K
  dot();dash();delay(1200); // A
  dot();dot();dot();delay(1200); // S
  dash();delay(2500);
}

// function to control LED buttons of butterflies
//void ledvlinders(invoer1,invoer2,invoer3,invoer4){
  
//}

void loop() {
  // put your main code here, to run repeatedly:
  // PART1: check temperatuur +led
  sensor.requestTemperatures();
  float temperatuur = sensor.getTempCByIndex(0);
  ledbytemp(temperatuur);
  Serial.print(temperatuur,1);
  Serial.println(" oC");
  delay(50);

  // PART2: morsecode
  //morsecode();

  // PART3: led vlinders
  invoer1 = digitalRead(invoerVlinder1);
  invoer2 = digitalRead(invoerVlinder2);
  invoer3 = digitalRead(invoerVlinder3);
  invoer4 = digitalRead(invoerVlinder4);
  
  // vlinder 1
  if (invoer1 == LOW){
    digitalWrite(LedVlinder1,HIGH);
  }
  else {
    digitalWrite(LedVlinder1,LOW);
  }

  // vlinder 2
  if (invoer2 == LOW){
    digitalWrite(LedVlinder2,HIGH);
  }
  else {
    digitalWrite(LedVlinder2,LOW);
  }

  // vlinder 3
  if (invoer3 == LOW){
    digitalWrite(LedVlinder3,HIGH);
  }
  else {
    digitalWrite(LedVlinder3,LOW);
  }

  // vlinder 4
  if (invoer4 == LOW){
    digitalWrite(LedVlinder4,HIGH);
  }
  else {
    digitalWrite(LedVlinder4,LOW);
  }

}
