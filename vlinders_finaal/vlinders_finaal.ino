int LedVlinder1 = 12;
int invoerVlinder1 = 4;
int LedVlinder2 = 10;
int invoerVlinder2 = 6;
int LedVlinder3 = 11;
int invoerVlinder3 = 7;
int LedVlinder4 = 13;
int invoerVlinder4 = 9;

int Speaker = 3;
int invoerBDG = 5;
int invoer2M = 8; 

int invoer1;
int invoer2;
int invoer3;
int invoer4;

int invoer5;
int invoer6;


void setup() {
  // put your setup code here, to run once:
  pinMode(LedVlinder1, OUTPUT);
  pinMode(invoerVlinder1,INPUT_PULLUP);
  pinMode(LedVlinder2, OUTPUT);
  pinMode(invoerVlinder2,INPUT_PULLUP);
  pinMode(LedVlinder3, OUTPUT);
  pinMode(invoerVlinder3,INPUT_PULLUP);
  pinMode(LedVlinder4, OUTPUT);
  pinMode(invoerVlinder4,INPUT_PULLUP);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  invoer1 = digitalRead(invoerVlinder1);
  invoer2 = digitalRead(invoerVlinder2);
  invoer3 = digitalRead(invoerVlinder3);
  invoer4 = digitalRead(invoerVlinder4);
  //Serial.println(invoer1);
  //Serial.println(invoer2);
  //Serial.println(invoer3);
  //Serial.println(invoer4);
  
  // vlinder 1
  if (invoer1 == LOW){
    digitalWrite(LedVlinder1,HIGH);
  }
  else {
    digitalWrite(LedVlinder1,LOW);
  }

  // vlinder 2
  if (invoer2 == LOW){
    digitalWrite(LedVlinder2,HIGH);
  }
  else {
    digitalWrite(LedVlinder2,LOW);
  }

  // vlinder 3
  if (invoer3 == LOW){
    digitalWrite(LedVlinder3,HIGH);
  }
  else {
    digitalWrite(LedVlinder3,LOW);
  }

  // vlinder 4
  if (invoer4 == LOW){
    digitalWrite(LedVlinder4,HIGH);
  }
  else {
    digitalWrite(LedVlinder4,LOW);
  }

}
