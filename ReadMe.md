Created by Ines Adriaens & Harmen Doekes  
Created on November, 30 2022
  
  

## Arduino project "butterflies"

Sinterklaas surprise "escape bucket" is an Arduino project in which we built an escape box with puzzles as surprise themed "(night) butterflies".



### Hardware Arduino

** Following components are needed for the arduino functionality**
- 2 Arduino nanos
- 6 LEDs
- temperature sensor
- 1 speaker
- 6 push buttons
- 2 resistances of 180 Ohm (variation: 2 LED in connected in series)
- 1 resistance of 470Ohm
- 2 breadboards
- jumper wires


### the BOX (non-electronic arduino hardware)
- box (ours was size 41x45x45cm) with a double bottom for the wiring and to hide the presents
- butterflies in paper for determination + determination charts with numbers according to the locks
- keylock(s) with keys with numbers
- keylock with key
- 


### Puzzle components

- Lock 1 = 2 butterflies + one song
	- 2 LED
	- SONG
	- in the song, one note is skipped. First the song needs to be 

### pictures

![image](pictures/escape_bucket.jpeg)