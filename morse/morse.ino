unsigned int MorsePin = 2;

void setup() {
  // put your setup code here, to run once:
  pinMode(MorsePin,OUTPUT);
}

void dot()
{
  digitalWrite(MorsePin, HIGH);
  delay(400);
  digitalWrite(MorsePin, LOW);
  delay(600);
}

void dash()
{
  digitalWrite(MorsePin, HIGH);
  delay(1200);
  digitalWrite(MorsePin, LOW);
  delay(600);
}

void loop() {
  // put your main code here, to run repeatedly:

  // TRAPKAST
  dash();dash();dot();delay(1200); // G
  dot();dash();delay(1200);  // A
  dash();dot();delay(1200); // N
  dash();dash();dot();delay(1200); // G
  dash();dot();dash();delay(1200); // K
  dot();dash();delay(1200); // A
  dot();dot();dot();delay(1200); // S
  dash();delay(2500);
}