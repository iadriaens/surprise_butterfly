int LedVlinder1 = 12;   // geen declaratie als analoge pin (bv A3 in setup: pinMode(A3,OUTPUT))
int invoerVlinder1 = 4;
int LedVlinder2 = 11;
int invoerVlinder2 = 6;
int LedVlinder3 = 10;
int invoerVlinder3 = 7;
int LedVlinder4 = 13;
int invoerVlinder4 = 9;

int invoer1;
int invoer2;
int invoer3;
int invoer4;


void setup() {
  // put your setup code here, to run once:

  // A0 tem A5 kna je als digitale pin gebruiken, A7 en A7 niet
  pinMode(LedVlinder1, OUTPUT);
  pinMode(invoerVlinder1,INPUT_PULLUP);
  pinMode(LedVlinder2, OUTPUT);
  pinMode(invoerVlinder2,INPUT_PULLUP);
  pinMode(LedVlinder3, OUTPUT);
  pinMode(invoerVlinder3,INPUT_PULLUP);
  pinMode(LedVlinder4, OUTPUT);
  pinMode(invoerVlinder4,INPUT_PULLUP);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  invoer1 = digitalRead(invoerVlinder1);
  invoer2 = digitalRead(invoerVlinder2);
  invoer3 = digitalRead(invoerVlinder3);
  invoer4 = digitalRead(invoerVlinder4);
  Serial.println(invoer1);
  Serial.println(invoer2);
  Serial.println(invoer3);
  Serial.println(invoer4);
  
  // vlinder 1
  if (invoer1 == LOW){
    digitalWrite(LedVlinder1,LOW);
  }
  else {
    digitalWrite(LedVlinder1,HIGH);
  }

  // vlinder 2
  if (invoer2 == LOW){
    digitalWrite(LedVlinder2,LOW);
  }
  else {
    digitalWrite(LedVlinder2,HIGH);
  }

  // vlinder 3
  if (invoer3 == LOW){
    digitalWrite(LedVlinder3,LOW);
  }
  else {
    digitalWrite(LedVlinder3,HIGH);
  }

  // vlinder 4
  if (invoer4 == HIGH){
    digitalWrite(LedVlinder4,HIGH);
  }
  else {
    digitalWrite(LedVlinder4,LOW);
  }

}
