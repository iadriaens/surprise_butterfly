/*
  Melody

  Plays a melody

  circuit:
  - 8 ohm speaker on digital pin 8

  created 21 Jan 2010
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/toneMelody
*/

#include "pitches.h"
int Speaker = 3;
int invoerBDG = 5;
int invoer2M = 8;

int invoer5;
int invoer6;

//int noteDurations[32]

// Noten voor verdronken vlinder
int vlinder_melody[] = {
  NOTE_C3, NOTE_D3, NOTE_E3,NOTE_E3,NOTE_E3,NOTE_E3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_E3,NOTE_D3,NOTE_D3,NOTE_D3,NOTE_E3,NOTE_E3,0,
  NOTE_C3, NOTE_D3, NOTE_E3,NOTE_E3,NOTE_E3,NOTE_E3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_E3,NOTE_D3,NOTE_D3,NOTE_D3,NOTE_E3,NOTE_E3,0,
  NOTE_E3,NOTE_E3,NOTE_A3,NOTE_A3,NOTE_A3,NOTE_B3,NOTE_C4,NOTE_B3,NOTE_A3,NOTE_G3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_G3,NOTE_A3,NOTE_G3,NOTE_F3,NOTE_E3,
  NOTE_D3,NOTE_D3,NOTE_D3,NOTE_E3,
  0,0,    //"rusten" 
  NOTE_D3,NOTE_C3,NOTE_B2,NOTE_B2,NOTE_B2,NOTE_C3,NOTE_D3,NOTE_D3,NOTE_D3,NOTE_D3,NOTE_E3,NOTE_E3,NOTE_E3,NOTE_F3,
  NOTE_E3,NOTE_E3,NOTE_E3,NOTE_D3,NOTE_E3,NOTE_E3,NOTE_E3,NOTE_F3,NOTE_E3,NOTE_E3,0,
  NOTE_E3,NOTE_E3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_D3,NOTE_D3,NOTE_D3,NOTE_E3,NOTE_E3,0,
  NOTE_C3,NOTE_D3,NOTE_E3,NOTE_E3,NOTE_E3,NOTE_E3,NOTE_G3,NOTE_G3,NOTE_G3,NOTE_G3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_F3,NOTE_E3,0
};

// Noten lengte verdronken vlinder
// note durations: 4 = quarter note, 8 = eighth note, etc.:

int vlinder_noteDurations[] = {
  8,8,8,8,8,8,8,8,8,8,8,8,8,8,4,2,
  8,8,8,8,8,8,8,8,8,8,8,8,8,8,4,2,
  8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
  16,16,8,8,8,8,8,8,8,8,2,
  8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,2
};

void play_vlinder() {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 110; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1400 / vlinder_noteDurations[thisNote];
    tone(Speaker, vlinder_melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(Speaker);
  }
}

// Noten voor motten
int motten_melody[] = {
  NOTE_D3,NOTE_D3,NOTE_E3,NOTE_G3,NOTE_B3,NOTE_B3,0,0,0,
  NOTE_B3,NOTE_B3,NOTE_A3,NOTE_G3,NOTE_B3,0,0,0,
  NOTE_D3,NOTE_E3,NOTE_G3,NOTE_B3,NOTE_B3,0,0,0,
  NOTE_B3,NOTE_B3,NOTE_C4,NOTE_D4,NOTE_A3,0,0,0,
  NOTE_A3,NOTE_A3,NOTE_B3,NOTE_C4,NOTE_C4,NOTE_C4,NOTE_C4,NOTE_C4,NOTE_C4,NOTE_C4,NOTE_A3,
  NOTE_B3,NOTE_B3,NOTE_B3,NOTE_B3,NOTE_B3,NOTE_B3,NOTE_B3,NOTE_C4,NOTE_CS4,NOTE_CS4,NOTE_CS4,NOTE_CS4,NOTE_CS4,NOTE_A3,NOTE_B3,NOTE_A3,
  NOTE_D4,NOTE_CS4,NOTE_C4,NOTE_B3,NOTE_B3,NOTE_A3,0,
  NOTE_D3,NOTE_D3,NOTE_E3,NOTE_G3,NOTE_B3,NOTE_B3,0,0,0,
  NOTE_B3,NOTE_B3,NOTE_A3,NOTE_G3,0,0,0,0, //Bas
  NOTE_A3,NOTE_A3,NOTE_B3,NOTE_C4,NOTE_B3,NOTE_D4,0,
  NOTE_D4,NOTE_E4,NOTE_B3,NOTE_B3,NOTE_G3
};

// Noten lengte twee motten
// note durations: 4 = quarter note, 8 = eighth note, etc.:

int motten_noteDurations[] = {
  4,6,16,4,8,8,4,2,4,
  6,16,4,4,4,4,2,4,
  4,4,4,8,8,4,2,4,
  4,6,16,4,4,4,2,4,
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,6,16,8,8,
  4,6,16,4,8,8,4,2,4,
  4,4,6,16,4,2,4,
  4,6,16,4,6,4.5,2,4,
  6,16,4,4,4
  
};

void play_motten() {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 96; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / motten_noteDurations[thisNote];
    tone(Speaker, motten_melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(Speaker);
  }
}


void setup() {
  pinMode(invoerBDG,INPUT_PULLUP);
  pinMode(invoer2M,INPUT_PULLUP);
}

void loop() {
  // check invoer 
  invoer5 = digitalRead(invoerBDG);
  invoer6 = digitalRead(invoer2M);

  if (invoer5 == LOW){
    play_vlinder();
  }
  //else {
    //noTone(Speaker);
  //}
  
  if (invoer6 == LOW){
    play_motten(); 
  }
  //else{
    //noTone(Speaker);
  //}
}
